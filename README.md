# Stately 

Stately is a simple, flexible, and highly readable state machine system for C#. 
It is ideal for constructing rigidly defined behaviors in objects for game engines
which use C# scripting, such as Unity.

### Usage

To use Stately in your application, simply include Stately/Stately.csproj in your
C# solution, or you can place the source files from Stately/src folder into
your project.

### Documentation

Robust documentation is hosted by Read the Docs:
https://stately-csharp.readthedocs.io

### License

Stately is licensed under GNU LGPLv3. Please see the included LICENSE file
for details.

### Donations

I have developed this tool on my own time.
If it helps you create a game, I'd love to hear about it,
and I also humbly request a donation:

<a href='https://ko-fi.com/O5O7J39B' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://az743702.vo.msecnd.net/cdn/kofi2.png?v=0' border='0' alt='Buy Me a Coffee at ko-fi.com' /></a>

### Special Thanks

Thanks to Seiji Tanaka for coming up with the original idea for this project
and writing an early version of the system.

Copyright (c) 2018 Evan Hemsley