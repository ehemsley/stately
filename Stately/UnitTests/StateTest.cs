﻿using NUnit.Framework;
using System;

namespace Stately
{
    namespace UnitTests
    {
        public class SampleStateMachine
        {
            public State rootState;
            public State idleState;
            public State jumpState;

            public SampleStateMachine()
            {
                rootState = new State("root");

                idleState = new State("idle");
                jumpState = new State("jump");

                rootState.StartAt(idleState);
            }

            public void Start()
            {
                rootState.Start();
            }

            public void Update(float deltaTime)
            {
                rootState.Update(deltaTime);
            }
        }

        [TestFixture]
        public class StateTest
        {
            // Properties

            [Test]
            public void CurrentState()
            {
                var stateMachine = new SampleStateMachine();

                stateMachine.Start();

                Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);
            }

            [Test]
            public void CurrentStateRecursive()
            {
                var stateMachine = new SampleStateMachine();

                var idleSubState = new State("idleSubState");

                stateMachine.idleState.StartAt(idleSubState);

                stateMachine.Start();

                Assert.AreSame(idleSubState, stateMachine.rootState.CurrentStateRecursive);
            }

            [Test]
            public void CurrentStatePath()
            {
                var stateMachine = new SampleStateMachine();

                var idleSubState = new State("idleSubState");

                stateMachine.idleState.StartAt(idleSubState);

                stateMachine.Start();

                Assert.AreEqual("root.idle.idleSubState", stateMachine.rootState.CurrentStatePath);
            }

            // Top Level Methods

            [Test]
            public void When_RootStateStarts_Then_CurrentStateIsSet()
            {
                var stateMachine = new SampleStateMachine();

                stateMachine.rootState.Start();

                Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);
            }

            [Test]
            public void When_RootStateReset_Then_ReturnsToStartingState()
            {
                var stateMachine = new SampleStateMachine();

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).AfterOneFrame();

                stateMachine.rootState.Start();
                stateMachine.rootState.Update(0.01f);

                Assert.AreSame(stateMachine.jumpState, stateMachine.rootState.CurrentState);

                stateMachine.rootState.Reset();

                Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);
            }

            // Callbacks

            [Test]
            public void When_StateEntered_Then_OnEnterIsExecuted()
            {
                var stateMachine = new SampleStateMachine();

                var number = 0;
                stateMachine.idleState.OnEnter = delegate
                {
                    number = 1;
                };

                stateMachine.Start();

                Assert.AreEqual(1, number);
            }

            [Test]
            public void When_StateUpdated_Then_OnUpdateIsExecuted()
            {
                var stateMachine = new SampleStateMachine();

                var number = 0;
                stateMachine.idleState.OnUpdate = delegate
                {
                    number += 1;
                };

                stateMachine.Start();
                stateMachine.Update(0.01f);
                stateMachine.Update(0.01f);

                Assert.AreEqual(2, number);
            }

            [Test]
            public void When_StateExited_Then_OnExitIsExecuted()
            {
                var stateMachine = new SampleStateMachine();
                var number = 0;

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).IfSignalCaught("jumpSignal");

                stateMachine.idleState.OnExit = delegate
                {
                    number = 1;
                };

                stateMachine.Start();
                stateMachine.rootState.SendSignal("jumpSignal");
                stateMachine.Update(0.01f);

                Assert.AreEqual(1, number);
            }

            [Test]
            public void When_TransitionExecutes_Then_CallbackExecutes()
            {
                var stateMachine = new SampleStateMachine();
                var number = 0;

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).IfSignalCaught("jumpSignal").ThenDo(delegate
                {
                    number = 1;
                });

                stateMachine.Start();
                stateMachine.rootState.SendSignal("jumpSignal");
                stateMachine.Update(0.01f);

                Assert.AreEqual(1, number);
            }

            // Single Transition Conditions

            [Test]
            public void When_IfConditionTransition_And_ConditionTrue_Then_SubstateChanges()
            {
                var stateMachine = new SampleStateMachine();
                var condition = false;

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).If(() => condition);

                stateMachine.Start();

                stateMachine.Update(0.01f);

                Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);

                condition = true;
                stateMachine.Update(0.01f);

                Assert.AreSame(stateMachine.jumpState, stateMachine.rootState.CurrentState);
            }

			[Test]
            public void When_IfConditionTransition_And_ConditionFalse_Then_SubstateDoesNotChange()
			{
				var stateMachine = new SampleStateMachine();
                var condition = false;

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).If(() => condition);

                stateMachine.Start();

                stateMachine.Update(0.01f);

                Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);
			}

            [Test]
            public void When_SignalCaughtTransition_And_SignalSent_Then_SubstateChanges()
            {
                var stateMachine = new SampleStateMachine();

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).IfSignalCaught("jumpSignal");

                stateMachine.Start();

                stateMachine.Update(0.01f);

                Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);

                stateMachine.rootState.SendSignal("jumpSignal");
                stateMachine.Update(0.01f);

                Assert.AreSame(stateMachine.jumpState, stateMachine.rootState.CurrentState);
            }

            [Test]
            public void When_AfterTransition_And_TimeHasPassed_Then_SubstateChanges()
            {
                var stateMachine = new SampleStateMachine();

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).After(1f);

                stateMachine.Start();

                stateMachine.Update(0.5f);

                Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);

                stateMachine.Update(0.5f);

                Assert.AreSame(stateMachine.jumpState, stateMachine.rootState.CurrentState);
            }

            [Test]
            public void When_AfterOneFrameTransition_And_OneFrameHasPassed_Then_SubstateChanges()
            {
                var stateMachine = new SampleStateMachine();

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).AfterOneFrame();

                stateMachine.Start();

                stateMachine.Update(0.01f);

                Assert.AreSame(stateMachine.jumpState, stateMachine.rootState.CurrentState);
            }

            [Test]
            public void When_AfterNFramesTransition_And_NFramesHavePassed_Then_SubstateChanges()
            {
                var stateMachine = new SampleStateMachine();

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).AfterNFrames(5);

                stateMachine.Start();

                for (var i = 0; i < 5; i++)
                {
                    Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);
                    stateMachine.Update(0.01f);
                }

                Assert.AreSame(stateMachine.jumpState, stateMachine.rootState.CurrentState);
            }

            // Any State Transitions

            [Test]
            public void When_AnyStateTransition_And_TransitionExecutes_Then_SubstateChanges()
            {
                var rootState = new State("root");
                var idleState = new State("idle");

                var redState = new State("red");
                var greenState = new State("green");
                var blueState = new State("blue");

                rootState.StartAt(idleState);
                rootState.Start();

                rootState.ChangeToSubState(redState).IfSignalCaught("red");
                rootState.ChangeToSubState(greenState).IfSignalCaught("green");
                rootState.ChangeToSubState(blueState).IfSignalCaught("blue");

                rootState.SendSignal("red");
                rootState.Update(0.01f);

                Assert.AreSame(redState, rootState.CurrentState);

                rootState.SendSignal("green");
                rootState.Update(0.01f);

                Assert.AreSame(greenState, rootState.CurrentState);

                rootState.SendSignal("blue");
                rootState.Update(0.01f);

                Assert.AreSame(blueState, rootState.CurrentState);
            }

            // Redefinition Methods

            [Test]
            public void When_OnTransitionTo_And_InsteadDo_Then_TransitionCallbackIsReplaced()
            {
                var stateMachine = new SampleStateMachine();
                var condition = false;
                var number = 0;

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).If(() => condition).ThenDo(delegate
                {
                    number = 1;
                });

                stateMachine.idleState.OnTransitionTo(stateMachine.jumpState).InsteadDo(delegate
                {
                    number = 2;
                });

                stateMachine.Start();

                condition = true;

                stateMachine.Update(0.01f);

                Assert.AreEqual(2, number);
            }


            [Test]
            public void When_ReplaceTransitionCondition_And_ConditionSatisfied_Then_TransitionExecutes()
            {
                var stateMachine = new SampleStateMachine();
                var condition = false;
                var newCondition = false;

                var number = 0;

                stateMachine.idleState.ChangeTo(stateMachine.jumpState).If(() => condition).ThenDo(delegate
                {
                    number = 1;
                });

                stateMachine.idleState.ReplaceTransitionCondition(stateMachine.jumpState).With.If(() => newCondition);

                stateMachine.Start();

                condition = true;
                stateMachine.Update(0.01f);

                Assert.AreSame(stateMachine.idleState, stateMachine.rootState.CurrentState);
                Assert.AreEqual(0, number);

                newCondition = true;
                stateMachine.Update(0.01f);

                Assert.AreSame(stateMachine.jumpState, stateMachine.rootState.CurrentState);
                Assert.AreEqual(1, number);
            }
        }
    }
}